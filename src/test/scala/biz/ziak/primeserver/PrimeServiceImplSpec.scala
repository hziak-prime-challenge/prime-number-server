package biz.ziak.primeserver

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.ActorSystem
import biz.ziak.primserver.{PrimeReply, PrimeRequest, PrimeServiceImpl}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration.DurationInt

class PrimeServiceImplSpec
  extends AnyWordSpec
    with BeforeAndAfterAll
    with Matchers
    with ScalaFutures {

  val testKit = ActorTestKit()

  implicit val patience: PatienceConfig = PatienceConfig(scaled(5.seconds), scaled(100.millis))

  implicit val system: ActorSystem[_] = testKit.system

  val service = new PrimeServiceImpl(system)

  override def afterAll(): Unit = {
    testKit.shutdownTestKit()
  }

  "PrimeService" should {
    "reply to single request" in {
      val reply = service.getPrimeNumbers(PrimeRequest(11))
      reply.futureValue should ===(PrimeReply(List(11, 7, 5, 3, 2)))
    }
  }
}