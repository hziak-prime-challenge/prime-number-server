package biz.ziak.math


import biz.ziak.math.Primes.{calcOddNumber, isPrime}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers.{be, convertToAnyMustWrapper}


class PrimeSpec
  extends AnyWordSpec
    with BeforeAndAfterAll
    with ScalaFutures {

  "Prime should" should {

    /*
     * Testing the edge case that the actually send number has to be in the result aswell
     */
    "testing primes up to 100" in {
      val primes: List[Int] = List(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97)
      primes.foreach { x =>
        val isP = isPrime(x)
        println(s"Number: $x is prime: $isP")
        isP must be(true)
      }
    }

    "test none primes up to 60" in {
      val nonPrimes: List[Int] = List(1, 4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30, 32, 33, 34, 35, 36, 38, 39, 40, 42, 44, 45, 46, 48, 49, 50, 51, 52, 54, 55, 56, 57, 58, 60, 62, 63)
      nonPrimes.foreach { x =>
        val isP = isPrime(x)
        println(s"Number: $x is prime: $isP")
        isP must be(false)
      }
    }

    "test odd numbers" in {
      val primes: List[Int] = List(2, 4, 8, 6, 10, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 80, 100)
      primes.foreach { x =>
        val odd = calcOddNumber(x)
        println(s"orginal number: ${x} new odd number: ${odd}")
        (odd % 2) must be(1)
      }

    }
  }
}




