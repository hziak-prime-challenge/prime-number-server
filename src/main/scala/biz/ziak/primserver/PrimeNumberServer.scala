package biz.ziak.primserver

//#import


import akka.ConfigurationException

import java.security.KeyStore
import java.security.SecureRandom
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import scala.io.Source
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.grpc.GrpcClientSettings
import akka.http.scaladsl.ConnectionContext
import akka.http.scaladsl.Http
import akka.http.scaladsl.HttpsConnectionContext
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import akka.pki.pem.DERPrivateKeyLoader
import akka.pki.pem.PEMDecoder
import biz.ziak.primserver.PrimeServiceHandler
import com.typesafe.config.ConfigFactory

import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success
import scala.concurrent.duration._
//#import


//#server
object PrimeNumberServer {

  def main(args: Array[String]): Unit = {
    val conf = ConfigFactory.defaultApplication()
    val system = ActorSystem[Nothing](Behaviors.empty, "PrimeNumberServer", conf)
    new PrimeNumberServer(system).run()
  }
}

class PrimeNumberServer(system: ActorSystem[_]) {
  def run(): Future[Http.ServerBinding] = {
    implicit val sys:ActorSystem[_] = system
    implicit val ec: ExecutionContext = system.executionContext
    val service: HttpRequest => Future[HttpResponse] =
      PrimeServiceHandler(new PrimeServiceImpl(system))
    val bound: Future[Http.ServerBinding] = Http(system)
      .newServerAt(interface = "127.0.0.1", port = 9001)
      .bind(service)
      .map(_.addToCoordinatedShutdown(hardTerminationDeadline = 10.seconds))
    bound.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        println("gRPC server bound to {}:{}", address.getHostString, address.getPort)
      case Failure(ex) =>
        println("Failed to bind gRPC endpoint, terminating system", ex)
        system.terminate()
    }
    bound
  }
}

