package biz.ziak.primserver

import akka.actor.typed.ActorSystem
import biz.ziak.math.Primes
import scala.concurrent.Future

class PrimeServiceImpl(system: ActorSystem[_]) extends PrimeService {
  private implicit val sys: ActorSystem[_] = system

  override def getPrimeNumbers(request: PrimeRequest): Future[PrimeReply] = {
    Future.successful(PrimeReply(Primes.calcPrimeNumbers(request.number)))
  }
}
