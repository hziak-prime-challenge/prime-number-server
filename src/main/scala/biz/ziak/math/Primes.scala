package biz.ziak.math

import scala.annotation.tailrec

/**
 * Helper class for prime number handling
 */
object Primes {
  private val numIterations = 20

  /**
   * returns all smaller or equal prime numbers of a given number
   * @param number
   * @return
   */
  @tailrec
  def calcPrimeNumbers(number: Int, numbers: List[Int] = List.empty[Int]): List[Int] = {
    if (number < 2) {
      numbers
    } else
      isPrime(number) match {
        case true =>
          calcPrimeNumbers(number - 1, numbers :+ number)
        case false =>
          calcPrimeNumbers(number - 1, numbers)
      }
  }

  @tailrec
  def MRPTest(d: Int, n: Int, x: Double): Boolean = {
    if (d == n - 1) {
      false
    } else {
      val xs = (x * x) % n
      if (xs == 1) {
        false
      } else if (xs == n - 1) {
        true
      } else {
        MRPTest(d * 2, n, xs)
      }
    }
  }

  /**
   * Checking for primality following the https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test method.
   * */
  def isPrime(number: Int): Boolean = {
    number match {
      case x if x < 2 => false
      case x if x == 4 => false
      case x if x <= 3 => true
      case x if (x % 2) == 0 => false
      case _ => {
        val d = calcOddNumber(number - 1)
        MRPTestLoop(d, number)
      }
    }
  }

  @tailrec
  def calcOddNumber(number: Int): Int = {
    if (number % 2 > 0) {
      number
    } else calcOddNumber(number / 2)
  }

  @tailrec
  protected def MRPTestLoop(d: Int, number: Int, counter: Int = numIterations): Boolean = {
    {
      val a = 2 + (Math.random % (number - 4)).toInt
      val xs = scala.math.pow(a, d) % number
      if (xs == 1 || xs == number - 1) {
        true
      } else {
        MRPTest(d, number, xs)
      }
    } match {
      case true =>
        true
      case false =>
        if (counter == 0) {
          false
        } else {
          MRPTestLoop(d, number, counter - 1)
        }
    }
  }


}

