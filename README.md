
# prime-number-server

A prime number server that waits for a gRPC input to return all prime numbers lower or equal to the given number.

## Implementation

In the research phase I came across the Akka gRPC toolkit. 
Since it covered everything in the requirements I used the available quickstart project als basis for the prime-number-server.
As a basis for prime testing I used the Miller–Rabin primality test.
In general the system has the same limitations and potentials for improvement as the proxy server.

## Usage

Run "sbt run" to start the server. The service will be reachable on localhost 9001.

## Open Task / Improvements

- Default values in application.conf with potential overwrite for env variables
- CI and docker compose
- Synchronize protobuf 

## Limitations

- only implemented for int32



